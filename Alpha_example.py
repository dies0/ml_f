# pip install scikit-learn
# pip install numpy 
# pip install k-means-constrained #might have to do it as admin

from sklearn.linear_model import LogisticRegression
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import cross_val_score
from sklearn import metrics
from sklearn.neural_network import MLPClassifier

from sklearn.cluster import KMeans
import numpy as np
import random
from itertools import islice
from k_means_constrained import KMeansConstrained

def ml_filter(matrixum,numww=1):
    resulutor = []
    numw=numww
    dbe=30
    matrixumm = [[h for h in i[:-1]] for i in matrixum]
    x_train, y_train, x_test, y_test = np.array([i[:] for i in matrixumm[:-numw]]),np.array([i[-1] for i in matrixum[:-numw]]),np.array([i[:] for i in [matrixumm[-numw]]]),np.array([i[-1] for i in [matrixum[-numw]]])
    ################################################################################
    # Instantiate
    tree_model = tree.DecisionTreeClassifier(max_depth=dbe)
    # Fit a decision tree
    tree_model = tree_model.fit(x_train, y_train)
    # Predictions/probs on the test dataset
    predicted = tree_model.predict(x_test)[0]
    resulutor.append([predicted,tree_model.score(x_train, y_train)])
    ################################################################################
    # Instantiate
    rf = RandomForestClassifier()    
    # Fit
    rf_model = rf.fit(x_train, y_train)
    # Predictions/probs on the test dataset
    predicted = rf_model.predict(x_test)[0]
    resulutor.append([predicted,rf_model.score(x_train, y_train)])
    ################################################################################
    # Instantiate
    svm_model = SVC(probability=True)
    # Fit
    svm_model = svm_model.fit(x_train, y_train)
    # Predictions/probs on the test dataset
    predicted = svm_model.predict(x_test)[0]
    resulutor.append([predicted,svm_model.score(x_train, y_train)])
    ################################################################################
    # instantiate
    knn_model = KNeighborsClassifier(n_neighbors=dbe)
    # fit the model
    knn_model.fit(x_train, y_train)
    # Predictions/probs on the test dataset
    predicted = knn_model.predict(x_test)[0]
    resulutor.append([predicted,knn_model.score(x_train, y_train)])
    ################################################################################
    # Instantiate
    bayes_model = GaussianNB()
    # Fit the model
    bayes_model.fit(x_train, y_train)
    # Predictions/probs on the test dataset
    predicted = bayes_model.predict(x_test)[0]
    resulutor.append([predicted,bayes_model.score(x_train, y_train)])
    ################################################################################
    # Instantiate
    MLP_model = MLPClassifier(learning_rate='adaptive',hidden_layer_sizes=(1000), activation='relu',shuffle=True, max_iter=300,n_iter_no_change=1000,verbose=False)
    # Fit the model
    MLP_model.fit(x_train, y_train)
    # Predictions/probs on the test dataset
    predicted = MLP_model.predict(x_test)[0]
    resulutor.append([predicted,MLP_model.score(x_train, y_train)])
    ###################################################################################
    return resulutor,y_test

def rebuilder(aa,imo):
    a = aa
    imoo = np.rot90(imo,1)
    origine = a[:,-1].reshape(imoo.shape[0],1)
    for i in range(len(imoo[0])):
        if i == 0:
            orix = np.append(origine , origine*imoo[:,i].reshape(imoo.shape[0],1), axis=1)
        else:
            orix = np.append(orix , orix[:,-1].reshape(imoo.shape[0],1)*imoo[:,i].reshape(imoo.shape[0],1), axis=1)
    return(np.rot90(orix,-1))

def data_converter_and_cleaner(initial_data):
    dataset = np.nan_to_num(np.rot90(np.array([(i[1:]/i[:-1]) for i in initial_data]),-1), copy=False, nan=1, posinf=1, neginf=1)
    dataset[dataset == 0] = 1
    return dataset

def predictor(dataset,block_size_to_predict_next_block=10,block_size_bein_predict=5):
    first_cluster_size = int(len(dataset)/1.8)
    first_cl = KMeansConstrained(n_clusters=first_cluster_size,size_min=1,size_max=2,random_state=0).fit(dataset)
    mydict = {i: dataset[np.where(first_cl.labels_ == i)[0]] for i in range(first_cl.n_clusters)}
            
    #minimum 2
    dataset_cluster = first_cl.predict(dataset)
    block_size_to_predict_next_block =10
    block_size_bein_predict =5

    #dataset_clustering_generator
    block_use_to_forecast  = np.array([dataset_cluster[i:i+block_size_to_predict_next_block] for i in range(0,len(dataset_cluster)-block_size_to_predict_next_block+1)])
    block_cluster_forecasted  = np.array([dataset_cluster[i:i+block_size_bein_predict] for i in range(0,len(dataset_cluster)-block_size_bein_predict+1)])

    #cluster_model
    cluster_size_of_forecast_block = [int(x) if x > 3 else 3 for x in [int(len(block_cluster_forecasted)/1.8)]][0]
    cluster_of_forecasted_block = KMeansConstrained(n_clusters=cluster_size_of_forecast_block,size_min=1,size_max=2,random_state=0).fit(block_cluster_forecasted)

    #clustering
    mydict2 = {i: block_cluster_forecasted[np.where(cluster_of_forecasted_block.labels_ == i)[0]] for i in range(cluster_of_forecasted_block.n_clusters)}

    #dataset
    dataseter = []
    dataset = []
    for i in range(len(block_use_to_forecast)):
        block_to_learn = np.array(block_use_to_forecast[i])
        block_to_predict = np.array([item for sublist in block_use_to_forecast[i+1:] for item in sublist][0:block_size_bein_predict])
        if len(block_to_predict) != block_size_bein_predict:
            pass
        else:
            dataseter.append([block_to_learn,block_to_predict])

    lsor = len(block_cluster_forecasted)
    block_to_predict_clustered = cluster_of_forecasted_block.predict(np.array([i[-1] for i in dataseter[:]], dtype='int32'))
    dataset = [np.append(dataseter[i][0],block_to_predict_clustered[i]) for i in range(len(block_to_predict_clustered))]
            
    #prediction_ml
    res1,_ = ml_filter(dataset,numww=1)

    #result_transformatiom
    result_first_method,class_f  = [[[[mydict[y][:,i].mean() for i in range(len(mydict[y][0]))] for y in mydict2[i[0]][z]] for z in range(len(mydict2[i[0]]))]for i in res1],res1
    return result_first_method,class_f

################################| Example code | ############################################################################
# #dummy dataset: initial_data is shape like[[x1,x2...xX]...[y1,y2...yY]] then get convert to [[x1,y1],[x2,y2]...]
# initial_data = np.array([[random.randint(0,20) for h in range(1000)] for i in range(4)]).astype(np.float)  
# dataset = data_converter_and_cleaner(initial_data)
# #Prediction
# result_first_method,class_f = predictor(dataset,block_size_to_predict_next_block=10,block_size_bein_predict=5)

# model={}
# for i in range(len(result_first_method)):
#     print(f"Result Model {i} |you will get 2 possible outcome. Can recover the model output in dict -> model , index: model[Model_{i}|")
#     print([rebuilder(initial_data[:,-10:],np.array(result_first_method[i][h])) for h in range(len(result_first_method[i]))])
#     model[f"Model_{i}"]=[rebuilder(initial_data[:,-10:],np.array(result_first_method[i][h])) for h in range(len(result_first_method[i]))]
################################################################################################################################
